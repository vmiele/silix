## ERASING BOOST:PROGRAM-OPTIONS from SYSTEM
sudo apt-get autoremove libboost-program-options-dev

## BUILDING BOOST STATIC LIBS
cd /tmp
wget http://sourceforge.net/projects/boost/files/boost/1.48.0/boost_1_48_0.tar.gz
tar zxvf boost_1_48_0.tar.gz
cd boost_1_48_0/

./bootstrap.sh --prefix=/tmp --with-libraries=program_options
./bootstrap.sh --show-libraries
./b2 install link=static

## BUILDING STATIC SILIX
cd /tmp
wget ftp://pbil.univ-lyon1.fr/pub/logiciel/silix/silix-1.2.5.tar.gz
tar zxvf silix-1.2.5.tar.gz 
cd silix-1.2.5


export LIBRARY_PATH=/tmp/lib
./configure CPPFLAGS=-I/tmp/include
make

