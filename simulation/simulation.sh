##### parameters #####
# k number of concatened mixnet networks to the first one
k=5000
export k
# f number of file to merge
f=10
export f
# i number of nodes per mixnet networks
i=500
export i

##### networks simulation #####
rm -f tmp3

#] 1 simulation
for j in `seq 0 $k`; do
export j
mixnet-sim test.model -n $i --undirected > tmp1
perl -ane 'if(int($F[0])>int($F[1])){print "$F[0]\t$F[1]\n"}else{print "$F[1]\t$F[0]\n"}' tmp1 | sort -n -k 1 -k 2 | uniq > tmp2
export j
perl -ane '$a=($ENV{i}*$ENV{j}+$F[0]);$b=($ENV{i}*$ENV{j}+$F[1]);if(int($F[0])>int($F[1])){print "$a\t$b\n"}else{print "$b\t$a\n"}' tmp2 | sort -n -k 1 -k 2  | uniq >> tmp3
done

#] reordering with shuffle
perl -ane 'BEGIN{use List::Util 'shuffle'; @shufid=shuffle(0..(($ENV{k}+1)*$ENV{i}-1))} $a=$shufid[$F[0]]; $b=$shufid[$F[1]]; if ($a>$b){print"$a\t$b\n"}else{print"$b\t$a\n"}' tmp3 | sort -n -k 1 -k 2 > network_n${i}_rep${k}.dat
# echo network_n${i}_rep${k}.dat created...

#3] spliting
# n number of lines
n=`wc -l  network_n${i}_rep${k}.dat | cut -d " " -f 1`
let "n=$n+$f-1"
let "m=$n/$f"
split -l $m network_n${i}_rep${k}.dat network_n${i}_rep${k}_

#4] .files output
ls -1 network_n${i}_rep${k}_* > network_n${i}_rep${k}.files
# echo network_n${i}_rep${k}.files created...

# cleaning
rm tmp*


##### buildfam #####
let "k1=$k+1"
let "n=$k1*$i"
../src/buildfam $n network_n${i}_rep${k}.files 2> network_n${i}_rep${k}.res
# echo network_n${i}_rep${k}.res created...
perl -ane 'BEGIN{use IO::File;$f=IO::File->new("network_n$ENV{i}_rep$ENV{k}.res");$l=<$f>;@z=split/\s+/,$l; print STDERR "BuilFamClass\n"; foreach $i(0..$#z){print STDERR "$i\t=\t$z[$i]\n"}} if (not((($z[$F[0]]<-1)and($z[$F[1]]==$F[0]))or(($z[$F[1]]<-1)and($z[$F[0]]==$F[1]))or($z[$F[0]]==$z[$F[1]]))){print $_}' network_n${i}_rep${k}.dat 2>  network_n${i}_rep${k}.class | wc -l >> simulation.res
tail -1 simulation.res
# echo network_n${i}_rep${k}.class created...
