/* FamXadj.h
 *
 * Copyright (C) 2009-2011 CNRS
 *
 * This file is part of SiLiX.
 *
 * Fam is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>
 */

#ifndef BUILDFAM_FAMXADJBOOST_H
#define BUILDFAM_FAMXADJBOOST_H
#include <string>
#include <vector>

namespace buildfam {

  class FamXadjBoost
  {
  private:
  public:
    // constructor for n nodes and edges given in file fname
    FamXadjBoost(int n, const std::string& fname);
    ~FamXadjBoost(){
    }
  };

}
#endif
