/* FamXadj.cc
 *
 * Copyright (C) 2009-2011 CNRS
 *
 * This file is part of SiLiX.
 *
 * Fam is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>
 */

#include<fstream>
#include<sstream>
#include<FamXadjBoost.h>

#include <boost/foreach.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/incremental_components.hpp>
#include <boost/pending/disjoint_sets.hpp>


using namespace std;
using namespace boost;

namespace buildfam {
  
  FamXadjBoost::FamXadjBoost(int n, const std::string& fname)
  {
    // init
    typedef adjacency_list <vecS, vecS, undirectedS> Graph;
    typedef graph_traits<Graph>::vertex_descriptor Vertex;
    typedef graph_traits<Graph>::vertices_size_type VertexIndex;
    
    const int VERTEX_COUNT = n;
    Graph graph(VERTEX_COUNT);
    
    std::vector<VertexIndex> rank(num_vertices(graph));
    std::vector<Vertex> parent(num_vertices(graph));
    
    typedef VertexIndex* Rank;
    typedef Vertex* Parent;
    
    disjoint_sets<Rank, Parent> ds(&rank[0], &parent[0]);
    
    initialize_incremental_components(graph, ds);
    incremental_components(graph, ds);
    
    graph_traits<Graph>::edge_descriptor edge;

    // file process
    ifstream xadjstream;
    string line;
    xadjstream.open(fname.c_str());
    if (!xadjstream.good()){
      cerr<<"Error in FamXadj : unable to open file "<<fname<<endl;
      exit(1);
    }
    
    int currline = 0;
    while (!xadjstream.eof()) {
      getline(xadjstream,line);
      if (line.length()){
	currline++;
	int i,j;
	istringstream linestream(line);
	string fromi;
	linestream >> fromi;
	if (fromi.length())
	  i = atoi(fromi.c_str());
	else{
	  cerr<<"Error in FamXadj : uncorrect format in line "<<currline<<endl;
	  exit(1);
	}	
	string toj;
	linestream >> toj;
	if (toj.length())
	  j = atoi(toj.c_str());
	else{
	  cerr<<"Error in FamXadj : uncorrect format in line "<<currline<<endl;
	  exit(1);
	}	
	if (i>j){
	  //boost::tie(edge, flag) = add_edge(i, j, graph);
	  ds.union_set(i,j);
	}
      }
    }
    xadjstream.close();  
    
    BOOST_FOREACH(Vertex current_vertex, vertices(graph)) {
      std::cout<< current_vertex << " <= " <<
	ds.find_set(current_vertex) << std::endl;
    }
    std::cout << std::endl;  
  }

}
