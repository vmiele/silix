#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sort_blast_mmap.h"

void Nieme_mot(char *string, int n, char *separat, char *result)
/* ==================================================================== */
/* retourne un pointeur sur le nieme mot d'une phrase, chaque mot       */
/* etant separe par un des caracteres contenus dans la chaine separat   */
/* ==================================================================== */

{

int ii;
char *mot;
char  *phrase;

if((phrase = (char*) malloc((1+strlen(string)) * sizeof(char))) == NULL){

        fprintf(stderr,"ERROR: not enough memory in function: Nth_word \n");
        exit(3);
        }

strcpy(phrase,string);



mot = strtok(phrase, separat);
for(ii = 1; ii < n; ii++) 
        if(mot != NULL)
                mot = strtok(NULL, separat);

if(mot != NULL) strcpy(result, mot);
else strcpy(result, "\0");

free(phrase);



return;
}
/******************************** Nth_word ***************************/
char *check_alloc(int nbrelt, int sizelt)
{
	char *retval;
	/*printf ("DEBUGGAGE check_alloc nb=%d size=%d:\n",nbrelt,sizelt);
	 * fflush(stdout);*/

	if( (retval=calloc(nbrelt,sizelt)) != NULL ){
		/*printf ("DEBUGGAGE check_alloc OK\n");
		 * fflush(stdout);*/
		return(retval);

	}
	/*printf ("DEBUGGAGE check_alloc ERROR\n");
	 * fflush(stdout);*/
	fprintf(stderr,"\nERROR: not enough memory (check_alloc).\n");
	exit(1);
}


#include <stdio.h>
#include <string.h>

void remLastBlank(char *mot)
{
    int i;

    i = strlen(mot) - 1;
    for(; i > 0; i--)
    {
        if (mot[i] != ' ')
            break;
        mot[i] = 0;
    }
}

void Maj(char *mot)
{
    int i;

    i = strlen(mot) - 1;
    for(; i > 0; i--)
    {
        mot[i] = toupper(mot[i]);
    }
}
#include <stdio.h>
#include <string.h>

void remLastWord(char sep,char *mot)
{
    int i;

    i = strlen(mot) - 1;
    for(; i > 0; i--)
    {
        if (mot[i] == sep){
	 mot[i] = '\0';
            break;
	    }
    }
}

#include <stdio.h>
#include <string.h>
#define MAXSP 2000


char *remplaceSpace(char *mot)
{
    int i,j,k;
    char mothtml[MAXSP];
    /*char *blanc ="&nbsp;";*/
    char *blanc ="#";
    j=0;
    for (i=0;i<=strlen(mot);i++){
    	if (mot[i] == ' '){
		for (k=0;k<strlen(blanc);k++){
		mothtml[j]=blanc[k];
		j++;
		if (j>=MAXSP){
			printf("Value of j too large.Please increase MAXSP.\nExit!\n");
			exit(1);
			}
		}
	}
	else {
	mothtml[j]=mot[i];
	j++;
	}
   }
      
      return(mothtml);
      
}      


void MixedCaseNoSpace(char *mot)
{
    int i;
    int flag;

    flag = 1;
    i = 0;
    for(; i < strlen(mot); i++)
    {
        if (mot[i] == ' '){
        	flag=0;
		mot[i]='_';
	}
	else{
		if (flag ==1){
			mot[i] = toupper(mot[i]);	
		}
		else {
			mot[i] = tolower(mot[i]);
		}
		flag=0;
	}
	
    }
}


void MixedCase(char *mot)
{
    int i;
    int flag;

    flag = 1;
    i = 0;
    for(; i < strlen(mot); i++)
    {
       
		if (flag ==1){
			mot[i] = toupper(mot[i]);
			flag=0;	
		}
		else {
			mot[i] = tolower(mot[i]);
		}
		flag=0;
	}
	
}
