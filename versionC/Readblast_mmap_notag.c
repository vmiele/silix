/*Avril 08
--------
on passe la longueur de nom des  sequences de 20 a 30
*/
#include "build_FAM_mmap.h"
#include <string.h>
struct BLAST_TOT bla;
struct BLAST_TOT bla2;

void reset_bla_hit(int num);
void reset_bla2_hit(int num);
extern char *p;
extern int debug;


void reset_bla(void)
{
int ii;

if(bla.nbHIT > 0) {
	for(ii = 0; ii < bla.nbHIT; ii++) reset_bla_hit(ii);
	free((char *) bla.HIT);
	}

strcpy(bla.query, "");
bla.lgq = -1;
bla.comp = -1;
bla.nbHIT = -1;
bla.status = 0;
strcpy(bla.comment, "\n");
bla.numq = -1;




}
/******************************** reset_bla ***************************/



void reset_bla_hit(int num)
{

if(bla.HIT[num].nbHSP > 0)
	free((char *) bla.HIT[num].HSP);

}
/******************************** reset_bla_hit ***************************/





void reset_bla2(void)
{
int ii;

if(bla2.nbHIT > 0) {
	for(ii = 0; ii < bla2.nbHIT; ii++) reset_bla2_hit(ii);
	free((char *) bla2.HIT);
	}

strcpy(bla2.query, "");
bla2.lgq = -1;
bla2.comp = -1;
bla2.nbHIT = -1;
bla2.status = 0;
strcpy(bla2.comment, "\n");
bla2.numq = -1;




}
/******************************** reset_bla2 ***************************/



void reset_bla2_hit(int num)
{

if(bla2.HIT[num].nbHSP > 0)
	free((char *) bla2.HIT[num].HSP);

}
/******************************** reset_bla2_hit ***************************/




void readblast_mmap(long deb, long fin)
{
long ii;
int jj;
int lg1, lg2;
int ll;
char ligne[100];
char charpos[10];
char flag[1];;

reset_bla();
reset_bla2();


bla.nbHIT = 1;
bla.status = 0;
bla.HIT = (struct BLAST_HIT *)check_alloc(bla.nbHIT,sizeof(struct BLAST_HIT));
bla.HIT[0].nbHSP = fin - deb + 1;
bla.HIT[0].HSP = (struct BLASTHSP *) check_alloc(
					bla.HIT[0].nbHSP,
					sizeof(struct BLASTHSP));
jj = 0;
for(ii=deb; ii <= fin; ii++) {
	
		
	/*for(ll=0; ll < 53; ll++){
	ligne[ll]=*(p+ii*53+ll);	;	 
	 }*/
	 
	/*for(ll=0; ll < 55; ll++){
	ligne[ll]=*(p+ii*55+ll);	;	 
	 }*/
	 
	for(ll=0; ll < (2*MAXSEQNAME+MAXLENINT+1); ll++){
	ligne[ll]=*(p+ii*(2*MAXSEQNAME+MAXLENINT+1)+ll);	;	 
	 }
	 
	ligne[ll-1]='\0';
	Nieme_mot(ligne, 3, "\n \t", charpos);
	
	/*fseek(blast_fic, blast_out[ii].pos, 0);*/
	fseek(blast_fic, atol(charpos), 0);
	/*fgets(ligne, 100, blast_fic);
	printf("%s\n",ligne);*/
	if(fscanf(blast_fic, "%s %d %d %s %d %d %d %f %d %d",
		/*flag,	*/
		bla.query,
		&(bla.HIT[0].HSP[jj].bq),
		&(bla.HIT[0].HSP[jj].eq),
		bla.HIT[0].sbjct,
		&(bla.HIT[0].HSP[jj].bs),
		&(bla.HIT[0].HSP[jj].es),
		&(bla.HIT[0].HSP[jj].sco),
		&(bla.HIT[0].HSP[jj].expt),
		&(bla.HIT[0].HSP[jj].id),
		&(bla.HIT[0].HSP[jj].pos)) != 10) {
		fprintf(stderr, "Format error in BLAST file !\n");
		fprintf(stderr, "error at position %ld\n",atol(charpos));
		fseek(blast_fic, atol(charpos), 0);
		fgets(ligne, 100, blast_fic);
		fprintf(stderr,"%s\n",ligne);
		exit(1);
		}
	lg1 = bla.HIT[0].HSP[jj].es - bla.HIT[0].HSP[jj].bs + 1;
	lg2 = bla.HIT[0].HSP[jj].eq - bla.HIT[0].HSP[jj].bq + 1;
	

		if(lg1 > lg2 ) bla.HIT[0].HSP[jj].lgHSP = lg1;
	else bla.HIT[0].HSP[jj].lgHSP = lg2;
	bla.HIT[0].HSP[jj].stat = 1;
	jj++;
	}
if (debug)
printf("debug readblast get_num_seq blas.query %s\n[%s]\n",bla.query,ligne);	
jj = get_num_seq(bla.query);
bla.numq = jj;
bla.lgq = db_seq[jj].lg;
bla.comp = db_seq[jj].comp;

if (debug)
printf("debug readblast get_num_seq bla.HIT[0].sbjct %s\n",bla.HIT[0].sbjct);	
jj = get_num_seq(bla.HIT[0].sbjct);
bla.HIT[0].lgs = db_seq[jj].lg;
bla.HIT[0].comp = db_seq[jj].comp;
bla.HIT[0].nums = jj;

}
/********************** end readblast ********************************/



void readblast_mmap_tab(long deb, long fin)
{
long ii;
int jj;
int lg1, lg2;
int ll;
char ligne[100];
char charpos[10];
char flag[1];
float percid;
int longaln;


reset_bla();
reset_bla2();


bla.nbHIT = 1;
bla.status = 0;
bla.HIT = (struct BLAST_HIT *)check_alloc(bla.nbHIT,sizeof(struct BLAST_HIT));
bla.HIT[0].nbHSP = fin - deb + 1;
bla.HIT[0].HSP = (struct BLASTHSP *) check_alloc(
					bla.HIT[0].nbHSP,
					sizeof(struct BLASTHSP));
jj = 0;
for(ii=deb; ii <= fin; ii++) {
	
		
	/*for(ll=0; ll < 53; ll++){
	ligne[ll]=*(p+ii*53+ll);	;	 
	 }*/
	 
	/*for(ll=0; ll < 55; ll++){
	ligne[ll]=*(p+ii*55+ll);	;	 
	 }*/
	 
	for(ll=0; ll < (2*MAXSEQNAME+MAXLENINT+1); ll++){
	ligne[ll]=*(p+ii*(2*MAXSEQNAME+MAXLENINT+1)+ll);	;	 
	 }
	 
	ligne[ll-1]='\0';
	Nieme_mot(ligne, 3, "\n \t", charpos);
	
	/*fseek(blast_fic, blast_out[ii].pos, 0);*/
	fseek(blast_fic, atol(charpos), 0);
	/*fgets(ligne, 100, blast_fic);
	printf("%s\n",ligne);*/
	/*5HT1B_FELCA	2	385	ADRB1_FELCA	12	389	521	1e-54	136	194	#9	*/	

	/*if(fscanf(blast_fic, "%s %d %d %s %d %d %d %f %d %d",
		bla.query,
		&(bla.HIT[0].HSP[jj].bq),
		&(bla.HIT[0].HSP[jj].eq),
		bla.HIT[0].sbjct,
		&(bla.HIT[0].HSP[jj].bs),
		&(bla.HIT[0].HSP[jj].es),
		&(bla.HIT[0].HSP[jj].sco),
		&(bla.HIT[0].HSP[jj].expt),
		&(bla.HIT[0].HSP[jj].id),
		&(bla.HIT[0].HSP[jj].pos)) != 10) {
		fprintf(stderr, "Format error in BLAST file !\n");
		fprintf(stderr, "error at position %ld\n",atol(charpos));
		fseek(blast_fic, atol(charpos), 0);
		fgets(ligne, 100, blast_fic);
		fprintf(stderr,"%s\n",ligne);
		exit(1);
		}*/
		
		
	/* sortie tabulee de blast -m 8*/	
	/*qid		sid		%ident	long.	nb mismatch	debut query	debut sujet	e-value
								nb gaps		fin query	fin sujet	bit
		*/
	/*5HT1B_FELCA	ADRB1_FELCA	34.34	396	230	9	2	385	12	389	1e-54	 205*/


	if(fscanf(blast_fic, "%s %s %f %d %*d %*d %d %d %d %d %f %d",
		bla.query,
		bla.HIT[0].sbjct,
		&percid,
		&longaln,
		&(bla.HIT[0].HSP[jj].bq),
		&(bla.HIT[0].HSP[jj].eq),
		&(bla.HIT[0].HSP[jj].bs),
		&(bla.HIT[0].HSP[jj].es),
		&(bla.HIT[0].HSP[jj].expt),
		&(bla.HIT[0].HSP[jj].sco)) != 10) {
		fprintf(stderr, "Format error in BLAST file !\n");
		fprintf(stderr, "error at position %ld\n",atol(charpos));
		fseek(blast_fic, atol(charpos), 0);
		fgets(ligne, 100, blast_fic);
		fprintf(stderr,"%s\n",ligne);
		exit(1);
		}
/* 	printf("%s %s %f %d %d %d %d %d %f %d\n", */
/* 	       bla.query, */
/* 	       bla.HIT[0].sbjct, */
/* 	       percid, */
/* 	       longaln, */
/* 	       (bla.HIT[0].HSP[jj].bq), */
/* 	       (bla.HIT[0].HSP[jj].eq), */
/* 	       (bla.HIT[0].HSP[jj].bs), */
/* 	       (bla.HIT[0].HSP[jj].es), */
/* 	       (bla.HIT[0].HSP[jj].expt), */
/* 	       (bla.HIT[0].HSP[jj].sco)); */
	bla.HIT[0].HSP[jj].id  = floor(percid * longaln / 100.0 + 0.5);/*= percid * longaln / 100.0;*/
	bla.HIT[0].HSP[jj].pos = bla.HIT[0].HSP[jj].id;	/* Ici il ya un pb, la valeur des pos n'est pas connue. Je prend la velur de id*/		
	lg1 = bla.HIT[0].HSP[jj].es - bla.HIT[0].HSP[jj].bs + 1;
	lg2 = bla.HIT[0].HSP[jj].eq - bla.HIT[0].HSP[jj].bq + 1;
	

		if(lg1 > lg2 ) bla.HIT[0].HSP[jj].lgHSP = lg1;
	else bla.HIT[0].HSP[jj].lgHSP = lg2;
	bla.HIT[0].HSP[jj].stat = 1;
	jj++;
	}
/*if (debug)
printf("debug readblast get_num_seq blas.query %s\n",bla.query);	*/
jj = get_num_seq(bla.query);
bla.numq = jj;
bla.lgq = db_seq[jj].lg;
bla.comp = db_seq[jj].comp;

/*if (debug)
printf("debug readblast get_num_seq bla.HIT[0].sbjct %s\n",bla.HIT[0].sbjct);	*/
jj = get_num_seq(bla.HIT[0].sbjct);
bla.HIT[0].lgs = db_seq[jj].lg;
bla.HIT[0].comp = db_seq[jj].comp;
bla.HIT[0].nums = jj;

}
/********************** end readblast_tab ********************************/
