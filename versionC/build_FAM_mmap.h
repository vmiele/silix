#include "sort_blast_mmap.h"
#define MAXFAMNAME 10
#define MAXFAMSEQ 40

#define PARTIAL_FAM      "HBG000000"
#define NEWFAM      "NEWFAM"


#define PART 0
#define COMP 1

#define FALSE 0
#define TRUE 1


#define REJECTED 0
#define CONTAIN 1
#define INCLUDED 2
#define GLOBAL 4
#define PAS 10


void load_seq_len(char *seq_part_Fname, char *seq_len_Fname);
void load_blast_out(char *BLAST_Fname);
int get_num_seq(char *mn);
int sort_db_seq(const void *a ,const void *b);
int sort_db_fam(const void *a ,const void *b);
int sort_blast_out(const void *a ,const void *b);
void build_fam(int test);
void readblast(int deb, int fin);
void analblast(int deb, int fin, int test);

void update_fam_comp(void);
void update_fam_part(void);
void renum_fam(void);
void add_fam_part(int famp, int num, int sco);
void realloc_famp(int num);

/*
int sort_lisfam(const void *a ,const void *b);
void update_FAM_SPAC(char *fic_name);
void push_fam(char *fic_name, char *fam_name, char *mn);
void get_fam(char *fic_name, char *mn, char *fam);
void select_new_fam_name(char *oldfam);
void concat_fam(char *fic_name, char *removed_fam, char *fam_kept);

*/


void reset_bla(void);
void reset_bla2(void);
void init_bla(void);
void printblast(void);
void Nth_word(char *string, int n, char *separat, char *result);
char *check_alloc(int nbrelt, int sizelt);


/* description of an HSP */
struct BLASTHSP {
	int sco;			/* Score */
	double expt;			/* Expect */
	int id;				/* Identities */
	int pos;			/* Positives */
	int lgHSP;			/* length of HSP */
	int bq, eq;			/* begin and end in query seq. */
	int bs, es;			/* begin and end in db seq. */
	int stat;			/* -1 if status undefined */
					/*  0 if bad HSP */
					/*  1 if OK */
	};

/* description of a BLAST hit */
struct BLAST_HIT {
	char sbjct[MAXSEQNAME];	/* db seq. name */
	int lgs;			/* db seq. length */
	short comp;			/* COMP if complete seq., else PART */
	short sel;			/* SELECTED if the hit fits criteria */
					/* of similarity, else REJECTED */ 
	int nbHSP;			/* number of HSPs */
	struct BLASTHSP *HSP;		/* list of HSPs */
	int nums;			/* numero dans db_seq */
	int sco;			/* somme des scores des HSP (apres */
	};                              /* suppression des overlaps et blocs */
                                        /* non consistants */

/* total description of a BLAST result */
struct BLAST_TOT {
	char query[MAXSEQNAME];		/* query seq. name */
	int lgq;			/* query seq. length */
	short comp;			/* COMP if complete seq., else PART */
	int nbHIT;			/* number of BLAST hits */
	struct BLAST_HIT *HIT;		/* list of BLAST hits */
	int status;			/* 0 if everything OK */
					/* 1 if BLAST search failed */
					/* 2 if file format problem */

	char comment[MAXSTRING];	/* message */
	int numq;			/* numero dans db_seq */
	};


extern struct BLAST_TOT bla;
extern struct BLAST_TOT bla2;



struct SEQ {
	char mn[MAXSEQNAME];
	int lg;
	int lgX;
	short comp;
	int fam;
	int *famp;
	int  nfamp;
	int scomax; /* pour les sequences partielles, score maxi */
	} ;
extern struct SEQ *db_seq;
extern int Nb_db_seq;

struct FAM {
	int nb;			/* nb de seq. dans la famille */
	int list[MAXFAMSEQ];	/* pointeur vers les MAXFAMSEQ 1eres seq de cette famille */
	};
extern struct FAM *db_fam; /* liste de toutes les familles */
extern int Nb_fam;


extern FILE * blast_fic;

struct BLAST_FIC_POS {
	char mn1[MAXSEQNAME];
	char mn2[MAXSEQNAME];
	long pos;
	};
extern struct BLAST_FIC_POS *blast_out;
/*extern long int Nb_blast_out;*/
extern  long Nb_blast_out;

extern float POSPERCENTSHMIN;
extern float FRAC_HSPSHLENMIN;
