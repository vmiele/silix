#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <limits.h> 	/* ULONG_MAX */
#include <errno.h> 	 /* errno */
#define MAXSEQNAME 30 	/* longueur de la chaine de charactere d'une sequence (pour homolens, 20 est trop petit) */
#define MAXLENINT 15	/* longueur de la chaine de charactere de la position dans le fichier BLAST*/
#define MAXSTRING 20000	/* longueur max d'une ligne dans le fichier balst re-formate*/
#define SORTSTRING 500	/* longueur max d'une ligne dans le sort*/
void* map_file(char * file, long * fsize, long  mem);
void write_blast(FILE * ,FILE *, FILE *);
int sort_blast_out(const void *a ,const void *b);
int sort_blast_out_2(const void *a ,const void *b);
void loadandsort_blast(char *BLAST_Fname, char *fout, long maxmem);
char *check_alloc(int nbrelt, int sizelt);
int interclassement(int ff, int nf);
void sort2files(char *, char  *,char  *);
