#include "sort_blast_mmap.h"
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>


/*====================================================================*/
/*========================= map_file =================================*/
/*====================================================================*/

void* map_file(char * file, long * fsize, long  memoire){
/* "memoire" est la taille du fichier definit par l'utilisateur
   C'est utile pour des tests.
   Si memoire est donnee comme 0, alors la memoire utilisee sera celle calculee par stats.
*/

	int fd;				/* descripteur du fichier*/
	void *pointeur;			/* le pointeur*/
	struct stat st;			/* attributs fichier*/
	FILE *tmp;

	/* tests sur le fichier*/

	if (stat(file,&st) != 0)  {
    		perror("Probleme d'ouverture du fichier");
    		return NULL;
    		}
   
   

	if ((tmp = fopen(file, "rw")) == NULL) {
		fprintf(stderr, "Erreur: impossible d'ouvrir/ecrire dans le fichier %s.\n", file);
		exit(1);
		}
	fd=fileno(tmp);
	   
	/* verifie si le fichier n'est pas nul */
	if (st.st_size  == 0) {
		fprintf(stderr,"Erreur: %s est vide.\nExit.",file);
		close(fd);
		return NULL;
		}     
	*fsize= st.st_size;
	printf("Taille du fichier : %ld\n",st.st_size);
	if (memoire == 0)
		{
		pointeur=mmap(NULL,st.st_size,PROT_READ|PROT_WRITE, MAP_PRIVATE, fd,0);
		}
		else
		{
		if (st.st_size != memoire) printf("Attention, la taille donnee par l'utilisateur (%ld) est differente de la taille  donne par stats(%ld).\n",memoire, st.st_size);
		pointeur=mmap(NULL,memoire,PROT_READ|PROT_WRITE, MAP_PRIVATE, fd,0);
		}
	
	if ( pointeur == (char*)MAP_FAILED ){
		fprintf(stderr,"Erreur dans mmap!(%s)\n",strerror(errno));
		exit(1);
		}
		else{
		printf("mmap ok.\n");
		}
	close(fd);	
	return(pointeur);	
	}


/*====================================================================*/
/*====================== sort_blast_out ==============================*/
/*====================================================================*/


int sort_blast_out(const void *a ,const void *b){
	
	int test;
	int ll,kk;
	char lignea[SORTSTRING],ligneb[SORTSTRING];
	char * a_pt = a;
	char * b_pt = b;
	
	if ((2*MAXSEQNAME + MAXLENINT) >= SORTSTRING) {
		fprintf(stderr,"La variable SORTSTRING est trop petite (%d). Recompiler le programe.\n",SORTSTRING);
		exit(1);
		}


	for(ll=0; ll <= (2*MAXSEQNAME + MAXLENINT); ll++){
		if (*(a_pt+ll) =='\0') {
			fprintf(stderr,"Erreur: fin du fichier TMP_BLAST! (a)\n");
			exit(1);
			}
		lignea[ll]=*(a_pt+ll);	 
	 	}
	
	lignea[ll-1]='\0';
	
	for(ll=0; ll < (2*MAXSEQNAME + MAXLENINT); ll++){
		if (*(b_pt+ll) =='\0') {
			printf("Erreur: fin du fichier TMP_BLAST! (b)\n");
			exit(1);
			}
		ligneb[ll]=*(b_pt+ll);	
	 	}
	ligneb[ll]='\0';	


	test = strcmp(lignea, ligneb); 
	return(test);
}

/*====================================================================*/
/*====================== sort_blast_out_2 ==============================*/
/*====================================================================*/

/* cette version de sort ne compare pas
les chaines 
"AAAAA1      BBBBB1  " et "AAAAA2      BBBBB2  " 
mais les chaines
"AAAAA1" et "AAAAA2" puis "BBBBB1" et "BBBBB2"
C'est ce qu'il faut utiliser pour rester coherent avec la version de build_FAM initiale ou 
le sort se fait sur un tableau dont les chaines ,n'on pas d'espace terminaux.
 
*/
int sort_blast_out_2(const void *a ,const void *b){

	
	int test;
	int ll,kk;
	char lignea[SORTSTRING],ligneb[SORTSTRING];
	char amn1[MAXSEQNAME],amn2[MAXSEQNAME];
	char bmn1[MAXSEQNAME],bmn2[MAXSEQNAME];

	char * a_pt = a;
	char * b_pt = b;
	if ((2*MAXSEQNAME + MAXLENINT) >= SORTSTRING) {
		fprintf(stderr,"La variable SORTSTRING est trop petite (%d). Recompiler le programe.\n",SORTSTRING);
		exit(1);
		}

	for(ll=0; ll <= (2*MAXSEQNAME + MAXLENINT); ll++){
		if (*(a_pt+ll) =='\0') {
			fprintf(stderr,"Erreur: fin du fichier TMP_BLAST! (a)\n");
			exit(1);
			}
		lignea[ll]=*(a_pt+ll);	 
	 	}
	
	lignea[ll-1]='\0';
	
	for(ll=0; ll < (2*MAXSEQNAME + MAXLENINT); ll++){
		if (*(b_pt+ll) =='\0') {
			printf("Erreur: fin du fichier TMP_BLAST! (b)\n");
			exit(1);
			}
		ligneb[ll]=*(b_pt+ll);	
	 	}
	ligneb[ll]='\0';	

	Nieme_mot(lignea, 1, " ", amn1);
	Nieme_mot(lignea, 2, " ", amn2);
	remLastBlank(amn1);
	remLastBlank(amn2);
	
	Nieme_mot(ligneb, 1, " ", bmn1);
	Nieme_mot(ligneb, 2, " ", bmn2);
	remLastBlank(bmn1);
	remLastBlank(bmn2);
	
	test = strcmp(amn1, bmn1);
	if (test == 0)  {
	test = strcmp(amn2, bmn2);	
		}
	return(test);
}


