#include "build_FAM_mmap.h"
#include <string.h>
#define HSPMIN  3 /* minimal length of an HSP */

struct BLAST_TOT bla;
struct BLAST_TOT bla2;


void init_bla2(void);
void consistentHSP(int num);
int sort_score(const void *a ,const void *b);
int sort_begin(const void *a ,const void *b);
void bla_to_bla2(int numHIT, int numHSPbla, int numHSPbla2);
void confrontHSP(int num, int ii, int jj);
void printblast2(void);
void remove_overlapq(int numHIT, int numHSP1, int numHSP2);
void remove_overlaps(int numHIT, int numHSP1, int numHSP2);
void summaryHIT(int num);
int max(int a, int b);
int min(int a, int b);

extern struct SEQ *db_seq;
extern int debug;




void analblast_mmap(long deb, long fin, int test)
{  
int ii;
int num;


readblast_mmap(deb, fin);

init_bla2();

/* remove or shorten non-consistent HSPs */
for(ii=0; ii < bla.nbHIT; ii++) {
	consistentHSP(ii);
	}

/* evaluate similarities */
for(ii=0; ii < bla.nbHIT; ii++) {
	summaryHIT(ii);
	}


return;
}
/********************** analblast_tab ********************************/


void analblast_mmap_tab(long deb, long fin, int test)
{  
int ii;
int num;


readblast_mmap_tab(deb, fin);

init_bla2();

/* remove or shorten non-consistent HSPs */
for(ii=0; ii < bla.nbHIT; ii++) {
	consistentHSP(ii);
	}

/* evaluate similarities */
for(ii=0; ii < bla.nbHIT; ii++) {
	summaryHIT(ii);
	}


return;
}
/********************** analblast ********************************/


void init_bla2(void) 
{
int ii;
strcpy(bla2.query, bla.query);
bla2.lgq = bla.lgq;
bla2.comp = bla.comp;
bla2.nbHIT = bla.nbHIT;
bla2.HIT = (struct BLAST_HIT *)check_alloc(bla.nbHIT,sizeof(struct BLAST_HIT));
bla2.status = bla.status;
strcpy(bla2.comment, bla.comment);
bla2.numq = bla.numq;

for(ii = 0; ii < bla.nbHIT; ii++) {
	strcpy(bla2.HIT[ii].sbjct, bla.HIT[ii].sbjct);
	bla2.HIT[ii].lgs = bla.HIT[ii].lgs;
	bla2.HIT[ii].comp = bla.HIT[ii].comp;
	bla2.HIT[ii].sel = bla.HIT[ii].sel;
	bla2.HIT[ii].nbHSP = bla.HIT[ii].nbHSP;
	bla2.HIT[ii].HSP = (struct BLASTHSP *) check_alloc(
					bla.HIT[ii].nbHSP,
					sizeof(struct BLASTHSP));
	bla2.HIT[ii].nums = bla.HIT[ii].nums;
	}


}
/********************** init_bla2 ********************************/

void consistentHSP(int num) 
{
int ii, jj;

/* sort HSPs by decreasing score */
qsort(	bla.HIT[num].HSP, 
	bla.HIT[num].nbHSP, 
	sizeof(bla.HIT[num].HSP[0]), 
	sort_score);

/* keep 1st HSP and initiate the others */
bla.HIT[num].HSP[0].stat = 1;
for(ii = 1; ii < bla.HIT[num].nbHSP; ii++)
	bla.HIT[num].HSP[ii].stat = -1;

/* check all HSPs for consistency */
for(ii = 0; ii < bla.HIT[num].nbHSP; ii++) {
	/* if an HSP has already been rejected: continue */
	if(bla.HIT[num].HSP[ii].stat == 0) continue;
	for (jj = ii+1; jj < bla.HIT[num].nbHSP; jj++) 
		confrontHSP(num, ii, jj);
	}

/* copy HSPs that are OK */
bla2.HIT[num].nbHSP = 0;
for(ii = 0; ii < bla.HIT[num].nbHSP; ii++) {
	if (bla.HIT[num].HSP[ii].stat == 0) continue;
	
	if (bla.HIT[num].HSP[ii].stat != 1) 
		fprintf(stderr, "WARNING !!!\n");

	bla_to_bla2(num, ii, bla2.HIT[num].nbHSP);

	bla2.HIT[num].nbHSP++;
	
	}



}
/********************** consistentHSP ********************************/


/* sort HSPs by decreasing score */

int sort_score(const void *a ,const void *b)
{
struct BLASTHSP *aa, *bb;

aa = (struct BLASTHSP *) a;
bb = (struct BLASTHSP *) b;

return (bb->sco - aa->sco); 

}
/********************** sort_score ********************************/


void bla_to_bla2(int numHIT, int numHSPbla, int numHSPbla2)
{

bla2.HIT[numHIT].HSP[numHSPbla2].sco   = bla.HIT[numHIT].HSP[numHSPbla].sco;
bla2.HIT[numHIT].HSP[numHSPbla2].expt  = bla.HIT[numHIT].HSP[numHSPbla].expt;
bla2.HIT[numHIT].HSP[numHSPbla2].id    = bla.HIT[numHIT].HSP[numHSPbla].id;
bla2.HIT[numHIT].HSP[numHSPbla2].pos   = bla.HIT[numHIT].HSP[numHSPbla].pos;
bla2.HIT[numHIT].HSP[numHSPbla2].lgHSP = bla.HIT[numHIT].HSP[numHSPbla].lgHSP;
bla2.HIT[numHIT].HSP[numHSPbla2].bq    = bla.HIT[numHIT].HSP[numHSPbla].bq;
bla2.HIT[numHIT].HSP[numHSPbla2].eq    = bla.HIT[numHIT].HSP[numHSPbla].eq;
bla2.HIT[numHIT].HSP[numHSPbla2].bs    = bla.HIT[numHIT].HSP[numHSPbla].bs;
bla2.HIT[numHIT].HSP[numHSPbla2].es    = bla.HIT[numHIT].HSP[numHSPbla].es;
}
/********************** bla_to_bla2 ********************************/


void confrontHSP(int num, int ii, int jj)
{
int bq1, eq1; /* begin and end of HSP1 (ii) in query seq. */
int bq2, eq2; /* begin and end of HSP2 (jj) in query seq. */
int bs1, es1; /* begin and end of HSP1 (ii) in sbjct seq. */
int bs2, es2; /* begin and end of HSP2 (jj) in sbjct seq. */
int ovq; /* <= 0 if HSP1 and HSP2 are overlapping in query seq. */
int ovs; /* <= 0 if HSP1 and HSP2 are overlapping in sbjct seq. */

/* if an HSP has already been rejected: return */
if(bla.HIT[num].HSP[jj].stat == 0) return;

eq1 = bla.HIT[num].HSP[ii].eq;
eq2 = bla.HIT[num].HSP[jj].eq;

bq1 = bla.HIT[num].HSP[ii].bq;
bq2 = bla.HIT[num].HSP[jj].bq;

es1 = bla.HIT[num].HSP[ii].es;
es2 = bla.HIT[num].HSP[jj].es;

bs1 = bla.HIT[num].HSP[ii].bs;
bs2 = bla.HIT[num].HSP[jj].bs;


/********************************************************
   reject HSPs that are located within another one.
   only the best HSP (highest score = HSP1) is kept.
*********************************************************/

if(bq1 >= bq2 && eq1 <= eq2)  {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}

if(bq2 >= bq1 && eq2 <= eq1)  {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}

if(bs1 >= bs2 && es1 <= es2)  {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}

if(bs2 >= bs1 && es2 <= es1)  {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}





/********************************************************
   reject HSPs that are in different orientation:
   Query:    ---- A ---- B -----
   Sbjct:    ---- B ---- A -----

   only the best HSP (highest score = HSP1) is kept.
*********************************************************/

if(((eq1 - eq2) * (es1 - es2)) <= 0) {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}

if(((bq1 - bq2) * (bs1 - bs2)) <= 0) {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}

/********************************************************
   calculate if HSPs are overlapping
*********************************************************/

ovq = (bq2 - eq1) * (eq2 - bq1);
ovs = (bs2 - es1) * (es2 - bs1);

/********************************************************
   accept non-overlapping HSPs in correct orientation
*********************************************************/

if(ovq > 0 && ovs > 0) {
	bla.HIT[num].HSP[jj].stat = 1;
	return;
	}


/********************************************************
   remove overlap between overlapping HSPs : keep the
   best HSP, and remove the part of the other that
   overlaps with it.
*********************************************************/

if(ovq < 0) {
	remove_overlapq(num, ii, jj);

	/* calculate if there is an overlap in sbjct seq */
	es1 = bla.HIT[num].HSP[ii].es;
	es2 = bla.HIT[num].HSP[jj].es;

	bs1 = bla.HIT[num].HSP[ii].bs;
	bs2 = bla.HIT[num].HSP[jj].bs;

	ovs = (bs2 - es1) * (es2 - bs1);
	}

if(ovs < 0) remove_overlaps(num, ii, jj);

/* filter out HSPs that are too short */

if(bla.HIT[num].HSP[jj].lgHSP < HSPMIN) {
	bla.HIT[num].HSP[jj].stat = 0;
	return;
	}

/* set status to 1 for consistent HSPs */
bla.HIT[num].HSP[jj].stat = 1;
return;
}
/********************** confrontHSP ********************************/


void printblast2(void)
{
int ii, jj;




for(ii = 0; ii < bla2.nbHIT; ii++) {
	printf("\tSBJCT= %s\n", bla2.HIT[ii].sbjct);
	printf("\tSBJCT length= %d\n", bla2.HIT[ii].lgs);
	printf("\t# HSPs= %d\n", bla2.HIT[ii].nbHSP);
	for(jj =0 ; jj < bla2.HIT[ii].nbHSP; jj++)
		printf("Sco = %d Exp = %g Id = %d Pos = %d Lg = %d query:%d..%d sbjct:%d..%d\n", 
			bla2.HIT[ii].HSP[jj].sco,
			bla2.HIT[ii].HSP[jj].expt,
			bla2.HIT[ii].HSP[jj].id,
			bla2.HIT[ii].HSP[jj].pos,
			bla2.HIT[ii].HSP[jj].lgHSP,
			bla2.HIT[ii].HSP[jj].bq,
			bla2.HIT[ii].HSP[jj].eq,
			bla2.HIT[ii].HSP[jj].bs,
			bla2.HIT[ii].HSP[jj].es);

	}



}
/******************************** printblast2 ***************************/


void remove_overlapq(int num, int ii, int jj)
{
int bq1, eq1; /* begin and end of HSP1 (ii) in query seq. */
int bq2, eq2; /* begin and end of HSP2 (jj) in query seq. */
int bs2, es2; /* begin and end of HSP2 (jj) in sbjct seq. */

int bqnew, eqnew; 	/* new begin and end in query seq. */
int bsnew, esnew; 	/* new begin and end in sbjct seq. */
int delta;	
float fract;

eq1 = bla.HIT[num].HSP[ii].eq;
eq2 = bla.HIT[num].HSP[jj].eq;

bq1 = bla.HIT[num].HSP[ii].bq;
bq2 = bla.HIT[num].HSP[jj].bq;

es2 = bla.HIT[num].HSP[jj].es;
bs2 = bla.HIT[num].HSP[jj].bs;


if(bq2 < bq1) {
	bqnew = bq2;
	eqnew = bq1 - 1;
	delta = eq2 - eqnew;
	bsnew = bs2;
	esnew = es2 - delta;
	}

else if (eq2 > eq1) {
	bqnew = eq1 + 1;
	eqnew = eq2;
	delta = bqnew - bq2;
	bsnew = bs2 + delta;
	esnew = es2;
	}

else {
	fprintf(stderr, "EXIT: unexpected case! (1)\n");
	exit(1);
	}

/* set new boundaries */
bla.HIT[num].HSP[jj].bq = bqnew;
bla.HIT[num].HSP[jj].eq = eqnew;

bla.HIT[num].HSP[jj].bs = bsnew;
bla.HIT[num].HSP[jj].es = esnew;

/* estimate new score, id, pos : */
/* = initial value * (fraction of length that has been preserved) */

fract = 1.0 - ((float) delta / (float) bla.HIT[num].HSP[jj].lgHSP);

bla.HIT[num].HSP[jj].sco = floor ((float) bla.HIT[num].HSP[jj].sco * fract);
bla.HIT[num].HSP[jj].id  = floor ((float) bla.HIT[num].HSP[jj].id  * fract);
bla.HIT[num].HSP[jj].pos = floor ((float) bla.HIT[num].HSP[jj].pos * fract);


/* calculate new length */

bla.HIT[num].HSP[jj].lgHSP = bla.HIT[num].HSP[jj].lgHSP - delta;

/* set expect value to -1 : this value should not be used after */
/* having changed HSPs boundaries */

bla.HIT[num].HSP[jj].expt = -1.0;


}
/******************************** remove_overlapq ***************************/


void remove_overlaps(int num, int ii, int jj)
{
int bq2, eq2; /* begin and end of HSP2 (jj) in query seq. */
int bs1, es1; /* begin and end of HSP1 (ii) in sbjct seq. */
int bs2, es2; /* begin and end of HSP2 (jj) in sbjct seq. */

int bsnew, esnew; 	/* new begin and end in query seq. */
int bqnew, eqnew; 	/* new begin and end in sbjct seq. */


int delta;	
float fract;

es1 = bla.HIT[num].HSP[ii].es;
es2 = bla.HIT[num].HSP[jj].es;

bs1 = bla.HIT[num].HSP[ii].bs;
bs2 = bla.HIT[num].HSP[jj].bs;

eq2 = bla.HIT[num].HSP[jj].eq;
bq2 = bla.HIT[num].HSP[jj].bq;


if(bs2 < bs1) {
	bsnew = bs2;
	esnew = bs1 - 1;
	delta = es2 - esnew;
	bqnew = bq2;
	eqnew = eq2 - delta;
	}

else if (es2 > es1) {
	bsnew = es1 + 1;
	esnew = es2;
	delta = bsnew - bs2;
	bqnew = bq2 + delta;
	eqnew = eq2;
	}

else {
	fprintf(stderr, "EXIT: unexpected case! (1)\n");
	exit(1);
	}

/* set new boundaries */
bla.HIT[num].HSP[jj].bs = bsnew;
bla.HIT[num].HSP[jj].es = esnew;

bla.HIT[num].HSP[jj].bq = bqnew;
bla.HIT[num].HSP[jj].eq = eqnew;

/* estimate new score, id, pos : */
/* = initial value * (fraction of length that has been preserved) */

fract = 1.0 - ((float) delta / (float) bla.HIT[num].HSP[jj].lgHSP);

bla.HIT[num].HSP[jj].sco = floor ((float) bla.HIT[num].HSP[jj].sco * fract);
bla.HIT[num].HSP[jj].id  = floor ((float) bla.HIT[num].HSP[jj].id  * fract);
bla.HIT[num].HSP[jj].pos = floor ((float) bla.HIT[num].HSP[jj].pos * fract);


/* calculate new length */

bla.HIT[num].HSP[jj].lgHSP = bla.HIT[num].HSP[jj].lgHSP - delta;

/* set expect value to -1 : this value should not be used after */
/* having changed HSPs boundaries */

bla.HIT[num].HSP[jj].expt = -1.0;



}
/******************************** remove_overlaps ***************************/

void summaryHIT(int num)
{
int delta_lg; /* length difference between aligned seq. */
int sco_tot, lg_tot, pos_tot, id_tot;
int ii;
float frac_HSPshlen; /* cumulative length of HSPs / shortest seq. length */
float frac_HSPlglen; /* cumulative length of HSPs / longest seq. length */
float frac_HSPavlen; /* cumulative length of HSPs / average seq. length */
float decal;         /* length difference / total seq. length */
float idpercent;     /* percent of identities within HSPs */
float pospercent;    /* percent of positives within HSPs */
float idpercentsh;   /* number of identities / shortest seq. length */
float pospercentsh;  /* number of positives / shortest seq. length */
int lgsX;	     /* length of the sbjct seq., without the masked residues */
int lgX;             /* number of masked residues */



/* sort HSPs by increasing position */
qsort(	bla2.HIT[num].HSP, 
	bla2.HIT[num].nbHSP, 
	sizeof(bla2.HIT[num].HSP[0]), 
	sort_begin);

/* calculate length difference :
            XXXXXXXXXXAAAAAAAXXX----BBBBBBBBXXXXXX
                  XXXXAAAAAAAXXXXXXXBBBBBBBBX
            ++++++              ++++         +++++ = length difference 
                       HSP1          HSP2    X = non-conserved residues

*/

/* determine the length of the database seq., without the masked residues */

if (debug)
printf("debug analablast get_num_seq %s\n",bla2.HIT[num].sbjct);
ii = get_num_seq(bla2.HIT[num].sbjct);
lgX = db_seq[ii].lgX;

if(bla2.HIT[num].lgs != db_seq[ii].lg)
	fprintf(stderr, 
	"WARNING: %s does not have the same length in files Masked_seq (%d aa) and in BLAST (%d aa) !!!\n", db_seq[ii].mn, db_seq[ii].lg, bla2.HIT[num].lgs);


lgsX = max(1, (bla2.HIT[num].lgs - lgX));




/* N-term */
delta_lg = abs(bla2.HIT[num].HSP[0].bq - bla2.HIT[num].HSP[0].bs);

/* C-term */
ii = bla2.HIT[num].nbHSP - 1;
delta_lg += abs((bla2.lgq - bla2.HIT[num].HSP[ii].eq) - 
                (bla2.HIT[num].lgs - bla2.HIT[num].HSP[ii].es));

/* internal gaps */
for(ii = 1; ii < bla2.HIT[num].nbHSP; ii++)
   delta_lg += abs((bla2.HIT[num].HSP[ii].bq - bla2.HIT[num].HSP[ii -1 ].eq) - 
                   (bla2.HIT[num].HSP[ii].bs - bla2.HIT[num].HSP[ii -1 ].es));

/* calculate total length, score, Identities and Positives : */
sco_tot = 0;
lg_tot = 0;
pos_tot = 0;
id_tot = 0;

for(ii = 0; ii < bla2.HIT[num].nbHSP; ii++) {
	sco_tot += bla2.HIT[num].HSP[ii].sco;
	lg_tot  += bla2.HIT[num].HSP[ii].lgHSP;
	pos_tot += bla2.HIT[num].HSP[ii].pos;
	id_tot  += bla2.HIT[num].HSP[ii].id;
	}

bla2.HIT[num].sco = sco_tot;

/*fprintf(stderr,"%d\n",bla2.HIT[num].sco);*/

/* 
frac_HSPlglen = (float) lg_tot / (float) max(bla2.HIT[num].lgs, bla2.lgq);
frac_HSPavlen = 2. * (float) lg_tot / (float) (bla2.HIT[num].lgs + bla2.lgq);
idpercent = (float) id_tot / (float) lg_tot;
pospercent = (float) pos_tot / (float) lg_tot;
idpercentsh = (float) id_tot / (float) min(bla2.HIT[num].lgs, bla2.lgq);
*/


/* si seuil similarite et longueur OK => on accepte alignement global */
/* NB: ici, on tient compte du filtrage */
frac_HSPshlen = (float) lg_tot / ((float) (lgsX + bla2.lgq)/ 2.);
decal = (float) delta_lg  / (float) (bla2.HIT[num].lgs + bla2.lgq);
pospercentsh = (float) pos_tot / ((float) (lgsX + bla2.lgq - lgX)/2.);


if(pospercentsh > POSPERCENTSHMIN &&
   frac_HSPshlen > FRAC_HSPSHLENMIN) {
	
	bla2.HIT[num].sel = GLOBAL;
	return;
	}



/* sinon, si  similarite pas OK on rejete */
pospercentsh = (float) pos_tot / lg_tot;
if(pospercentsh < POSPERCENTSHMIN) {
	bla2.HIT[num].sel = REJECTED;
	return;
	}


/* si similarite OK et query incluse dans HSP alors INCLUDED */

frac_HSPshlen = (float) lg_tot / bla2.lgq;
if(frac_HSPshlen > FRAC_HSPSHLENMIN) {
  bla2.HIT[num].sel = INCLUDED;
	return;
	}

/* si similarite OK et sbjct incluse dans HSP alors CONTAIN */

frac_HSPshlen = (float) lg_tot / bla2.HIT[num].lgs;
if(frac_HSPshlen > FRAC_HSPSHLENMIN) {
	bla2.HIT[num].sel = CONTAIN;
	return;
	}

/* autre cas: on rejete */

bla2.HIT[num].sel = REJECTED;

/*
printf("#%-16.16s %d aa %-16.16s   %d  aa  sco= %d lgHSP/shlg = %.3f decal= %.3f %%pos/shlg= %.3f \n",
	bla2.query,
	bla2.lgq,
	bla2.HIT[num].sbjct,
	bla2.HIT[num].lgs,
 	sco_tot,
	frac_HSPshlen,
	decal,
	pospercentsh);

*/

        
/*
printf("q:%s\t%d\t%s\t%d\t%d\t%d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
	bla2.query,
	bla2.lgq,
	bla2.HIT[num].sbjct,
	bla2.HIT[num].lgs,
 	sco_tot,
	lg_tot,
	frac_HSPshlen,
	frac_HSPlglen,
	frac_HSPavlen,
	decal,
	idpercent,
	pospercent,
	idpercentsh,
	pospercentsh);
*/

}
/******************************** summaryHIT ***************************/


/* sort HSPs by increasing position (begin point) */

int sort_begin(const void *a ,const void *b)
{
struct BLASTHSP *aa, *bb;

aa = (struct BLASTHSP *) a;
bb = (struct BLASTHSP *) b;

return (aa->bq - bb->bq); 

}
/********************** sort_begin ********************************/

int min(int a, int b)
{
if (a < b) return(a);
return(b);
}
/********************** min ********************************/

int max(int a, int b)
{
if (a > b) return(a);
return(b);
}
/********************** max ********************************/


