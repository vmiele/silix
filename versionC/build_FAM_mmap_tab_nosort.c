#include "build_FAM_mmap.h"
#include "sort_blast_mmap.h"
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>

/* Version de Laurent dans lquelle Nth_word est remplace par Nieme_mot*/
/*/bge/duret/bact/src/4_class_fam/*/
/*
cc -g -o build_FAM build_FAM.c misc.c Readblast.c Analblast.c -lm
cc    -o build_FAM build_FAM.c misc.c Readblast.c Analblast.c -lm


/* Mofif Juin 05 : on remplace le ET (&&)  par le OU (||)  dans 
 les conditions de rejet des sequen ce partielles


 Juillet 06
 -----------
 utilisation de mmap pour eviter de tout charger en memoire...
 
 
 

 Juin 2007
 ---------
 On utilise la sortie blast tabul�e... 
 
 Sortie BLAST tabulee
 
MOUSE10_10_PE1	MOUSE10_10_PE1	100.00	146	0	0	1	146	1	146	6e-85	 308
MOUSE10_10_PE2	MOUSE10_10_PE2	100.00	173	0	0	1	173	1	173	2e-100	 360
MOUSE10_10_PE2	MOUSE7_105_PE14	45.10	51	28	0	9	59	1203	1253	1e-07	52.4
MOUSE10_10_PE2	MOUSE7_105_PE2	33.64	107	58	2	6	103	11	113	2e-07	52.0
MOUSE10_10_PE2	MOUSE9_106_PE1	36.67	60	38	0	6	65	897	956	3e-07	50.8
MOUSE10_10_PE2	MOUSE5_143_PE8	40.00	60	36	0	6	65	912	971	3e-07	50.8
 
 
 Juillet 2008
 ------------
On passe d'une longueur de nom de sequence de 20 a 30  
   
 Aout 2008
 ----------
- Les longeurs des sequences et de l'entier sont d�finis dans  le .h

- Le classement alphabetique du pointeur fonctionne correcetement




/*
build_FAM low_complexity.test partial.mn.test BLAST_TOT.test FAM_SPAC.test 0.5 0.8  100 0.5
*/


struct SEQ *db_seq;
int Nb_db_seq;
struct FAM *db_fam; /* liste de toutes les familles */
int Nb_fam;


FILE * blast_fic;

struct BLAST_FIC_POS *blast_out;
/*long int Nb_blast_out;*/
long Nb_blast_out;

float POSPERCENTSHMIN;
float FRAC_HSPSHLENMIN;
int minplen;
float minpov;

char* BLAST_TMP = "TABTMPBLAST";
char* SORTBLAST_TMP = "TABSORTTMPBLAST";
FILE *blast_tmp;

FILE * blast_sort;


char *p;
long memoire;
int debug = 0;


/*====================================================================*/
/*======================== print_mess  ===============================*/
/*====================================================================*/

void print_mess(void)
{
fprintf(stderr, 
"                                                                        \n\
build_FAM: analyses BLAST outputs and updates the FAM_SPAC of HOBACGEN.  \n\
                                                                         \n\
                                                                         \n");
fprintf(stderr,
"Usage: build_FAM_mmap_tab DB_seq Partial_seq blast FAM_SPAC sim overlap minplen minpov\n\
                                                                         \n\
	DB_seq       : file containing the names, lengths and masked     \n\
                       lengths of each sequence of the entire database.  \n\
	Partial_seq  : file containing the list of partial sequences     \n\
	blast        : output tabulated BLAST file                       \n\
	FAM_SPAC     : name of the output FAM_SPAC file                  \n");
	
fprintf(stderr, 
"       sim          : min %% similarity to classify in a same family    \n\
	overlap      : min %% overlap to classify in a same family       \n\
	minplen      : min length to include a partial seq. in the       \n\
                       classification                                     \n\
	minpov       : min %% overlap to include a partial seq. in the  \n\
                       classification\n");
fprintf(stderr, 
"                                                                         \n\
NB: a partial seq. is included in the classification IF it is longer than \n\
\"minplen\" OR if it overlaps at least \"minpov\" %% of a complete seq.\n\
                                                                         \n\
Example:                                                                \n\
build_FAM_mmap_tab low_complexity.test partial.mn.test BLAST_TOT.test FAM_SPAC.test 0.5 0.8 100 0.5\n\
Version 5: 14/08/08 \n\
");

exit(1);
}


/*====================================================================*/
/*=========================== main ===================================*/
/*====================================================================*/

main(int argc,char *argv[])
{  
	FILE *outfile;
	int ii, jj;

	if(argc != 9) print_mess();

	printf("Load sequences....\n");
	load_seq_len(argv[2], argv[1]);
	printf("OK\n\n");


	/* 
	Charge le fichier de BLAST tabule et cree un pointeur associe classe dans l'odre alphabetique                               
	---------------------------------------------------------------------------------------------  
	*/

	printf("Load blast-tabulated  file and generate a well structured tabulated file and corresponding pointeur ....\n");
	load_blast_out(argv[3]);
	printf("OK\n\n");

	POSPERCENTSHMIN = atof(argv[5]);
	FRAC_HSPSHLENMIN = atof(argv[6]);
	minplen = atoi(argv[7]);
	minpov = atof(argv[8]);

	printf("\
min %% similarity to classify in a same family: 		%.3f\n\
min %% overlap to classify in a same family:     		%.3f\n\
min length to include a partial seq. in the classification :	%d\n\
OR min %% overlap to include a partial seq. in the classification : %.3f\n\
", POSPERCENTSHMIN, FRAC_HSPSHLENMIN, minplen, minpov);
	fflush(stdout);

	/* Construit les familles a partir des sequences completes  */
	/* -------------------------------------------------------- */

	printf("Build families from completes....\n");
	build_fam(COMP);
	printf("OK\n\n");

	/* Renumerote les familles */
	/* ----------------------- */

	printf("Renum families ");
	renum_fam();
	printf("OK\n\n");

	/* Construit les familles a partir des sequences partielles  */
	/* -------------------------------------------------------- */
	printf("Build families with partials....\n");
	build_fam(PART);
	printf("OK\n\n");


	/* unmmapp le pointeur et ferme ke fichier temporaire */
	/* ---------------------------------------------------*/
	
	munmap(p,memoire);	
	fclose(blast_tmp);

	/* Classe les familles */
	/* ------------------- */

	qsort(	db_seq, 
	Nb_db_seq, 
	sizeof(db_seq[0]), 
	sort_db_fam);

	if ((outfile = fopen(argv[4], "w")) == NULL) {
		fprintf(stderr, "Unable to create file %s \n", argv[4]);
		exit(1);
		}

	for(ii=0; ii<Nb_db_seq; ii++) {
		if(db_seq[ii].fam != 0)
			fprintf(outfile, "HBG%6.6d\t%s\n", 
				db_seq[ii].fam, db_seq[ii].mn);

		else if(db_seq[ii].nfamp == 0) 
			fprintf(outfile, "HBG%6.6d\t%s\tpartial\n", 
				db_seq[ii].fam, db_seq[ii].mn);
		else for(jj=0; jj<db_seq[ii].nfamp; jj++) 
			fprintf(outfile, "HBG%6.6d\t%s\tpartial\n", 
				db_seq[ii].famp[jj], db_seq[ii].mn);

		}

	printf("Normal program end!\n");
	exit(0);
}



/*====================================================================*/
/*========================load_seq_len ===============================*/
/*====================================================================*/


/* gets the list of db sequences, their length, masked length and
   whether they are partial or complete */

void load_seq_len(char *seq_part_Fname, char *seq_len_Fname)
{
	char string[101];
	char mn[MAXSEQNAME];
	int lg, lgX, num;

	FILE *seq_part;
	FILE *seq_len;
	int ii, jj, kk;

	Nb_db_seq = 0;

	if ((seq_part = fopen(seq_part_Fname, "r")) == NULL) {
		fprintf(stderr, "Unable to open file %s \n", seq_part_Fname);
		exit(1);
		}

	if ((seq_len = fopen(seq_len_Fname, "r")) == NULL) {
		fprintf(stderr, "Unable to open file %s \n", seq_len_Fname);
		exit(1);
		}

	while(fgets(string, 100, seq_len) != NULL) Nb_db_seq++;

	printf("Sequences...\n");
	db_seq = (struct SEQ *)check_alloc(Nb_db_seq,sizeof(struct SEQ));
	rewind(seq_len);

	/* au depart, on cree autant de familles que de sequences  + 1 pour les partielles */
	Nb_fam = Nb_db_seq + 1;
	printf("Families...\n");
	db_fam = (struct FAM *)check_alloc(Nb_fam, sizeof(struct FAM));


	ii = 0;
	lgX = 0;
	while(fgets(string, 100, seq_len) != NULL) {
	  /*if(sscanf(string, "%s %d %d", mn, &lg, &lgX) != 3) {*/
	  if(sscanf(string, "%s %d", mn, &lg) != 2) {
	    fprintf(stderr, "EXIT: format error in %s", seq_len_Fname);
	    exit(1);
	  }
	  strcpy(db_seq[ii].mn, mn);
	  db_seq[ii].lg = lg;
	  db_seq[ii].lgX = lgX;
	  db_seq[ii].comp = COMP;
	  db_seq[ii].nfamp = 0;
	  db_seq[ii].scomax = 0;
	  ii++;
	}
	
	/* sort seq. by alphabetical order */
	qsort(	db_seq, 
		Nb_db_seq, 
		sizeof(db_seq[0]), 
		sort_db_seq);


	for(ii=0; ii < Nb_db_seq; ii++) {
		jj = ii+1;
		db_seq[ii].fam = jj;
		db_fam[jj].nb = 1;
		db_fam[jj].list[0] = ii;
		for(kk=1; kk < MAXFAMSEQ; kk++) db_fam[jj].list[kk] = -1;
		}

	db_fam[0].nb = 0;

	while(fgets(string, 100, seq_part) != NULL) {
		Nieme_mot(string, 1, "\n \t", mn);
		num = get_num_seq(mn);
		db_seq[num].comp = PART;
		jj = db_fam[0].nb;
		if(jj < MAXFAMSEQ) db_fam[0].list[jj] = num;
		db_fam[0].nb = jj + 1;
		jj = db_seq[num].fam;
		db_fam[jj].nb = db_fam[jj].nb - 1;
		db_seq[num].fam = 0;
		}

	fclose(seq_part);
	fclose(seq_len);

}


/*====================================================================*/
/*======================== sort_db_seq ===============================*/
/*====================================================================*/

/* sort seq. by alphabetical order */

int sort_db_seq(const void *a ,const void *b)
{
	struct SEQ *aa, *bb;

	aa = (struct SEQ *) a;
	bb = (struct SEQ *) b;

	return (strcmp(aa->mn, bb->mn)); 

}


/*====================================================================*/
/*======================== get_num_seq ===============================*/
/*====================================================================*/

/* identifies a sequence in db_seq. exits if sequence not found */

int get_num_seq(char *mn)
{
	int ii, deb2, fin2, pos, diff;

	/* recherche dichotomique */
	ii = 0;
	deb2 = 0;
	fin2 = Nb_db_seq -1;

	while(deb2 <= fin2) {
		pos = (deb2 + fin2)/2;
		diff = strcmp(mn, db_seq[pos].mn);
		if(diff<0)  fin2 = pos -1;
		else if(diff>0) deb2 = pos +1;
		else return(pos);
		}

	fprintf(stderr, "EXIT: %s is not found in Masked_seq file !\n", mn);
	exit(1);

}

/*====================================================================*/
/*====================== load_blast_out ==============================*/
/*====================================================================*/

void load_blast_out(char *BLAST_Fname)
{
	long ii;
	long pos;
	char mn1[MAXSEQNAME];
	char mn2[MAXSEQNAME];
	char buffer[MAXSTRING];
	long taillefic;


	Nb_blast_out = 0;


	if ((blast_fic = fopen(BLAST_Fname, "r")) == NULL) {
		fprintf(stderr, "Unable to open file %s \n", BLAST_Fname);
		exit(1);
		}


	if ((blast_tmp = fopen(BLAST_TMP, "w")) == NULL) {
		fprintf(stderr, "Unable to open write file %s \n", BLAST_TMP);
		exit(1);
		}

	ii = 0;
	pos = 0;

	/* Ecrit un fichier temporaire BLAST_TMP */
	/* ------------------------------------- */

	while(fgets(buffer,sizeof(buffer),blast_fic) ) {
   		if(sscanf(buffer, 
			"%s %s", 
			mn1, 
			mn2) != 2) {
				fprintf(stderr, "EXIT: Format error in  %s :\n",BLAST_Fname);
				fprintf(stderr, "[%s]\n",buffer);
				exit(1);
				} 
   	fprintf(blast_tmp,"%-*s%-*s%*ld\n",MAXSEQNAME,mn1,MAXSEQNAME,mn2,MAXLENINT,pos); /* Format ! 2 * MAXSEQNAME + 12 +1 = 53 characteres. Non 2 * 30 +15 +1 */
  	pos = ftello(blast_fic);
   	ii ++;
   	}

	fflush(blast_tmp);
	Nb_blast_out=ii;
	printf("Number of Blast Hits (%lld lines )...\n",Nb_blast_out);
	printf("Last Line= %-*s%-*s%*lld\n",MAXSEQNAME,mn1,MAXSEQNAME,mn2,MAXLENINT,pos);

	
	
	fclose(blast_tmp);	/* Fermeture du fichier temporaire (il sera ouvert par map_file)*/
	
	/* Cree un pointeur mape sur le fichier temporaire */
	/* ----------------------------------------------- */
	
	
	memoire=Nb_blast_out *(2*MAXSEQNAME+MAXLENINT+1);
	printf("Memory needed of file %s = %ld.\n",BLAST_TMP,memoire);
	
	printf("Create pointer mapped to the file.\n");
	p = map_file(BLAST_TMP, &taillefic,0);	
	
	printf("Real size of file %s = %ld.\n",BLAST_TMP,taillefic);
	
	if (taillefic != memoire) {
		fprintf(stderr,"EXIT: File sizes given by the stats and calculated differ.\n");
		exit(1);
		}
		
	printf("Sorting pointer SKIPPED!!!!!.\n");
	
	/* Classement sur le pointeur SUPPRIME */
	/* -------------------------- */
	/*qsort(	p, 
	Nb_blast_out, 
	(2*MAXSEQNAME+MAXLENINT+1), 
	sort_blast_out_2);*/

	/* Ecriture du fichier temporaire classe */
	/* ------------------------------------- */
	/*fprintf(sortblast_tmp,"%s",p);
	fflush(stdout);	
	munmap(p,memoire);
	fclose(sortblast_tmp); 
	printf("OK.\n");
	fflush(stdout);*/
	
	
}


/*====================================================================*/
/*========================= build_fam ================================*/
/*====================================================================*/

void build_fam(int test)
{
	long ii, kk, first, jj, ll;
	int fd;
	char ligne[100];
	long memoire;
	long taillefic;
	char mn1[MAXSEQNAME];
	char mn2[MAXSEQNAME];
	char firstmn1[MAXSEQNAME];
	char firstmn2[MAXSEQNAME];
	
	first = 0;
	printf("Begin buildfam...\n");

	jj=0;
	kk=0;
	ll=0;
	
	for(ii=0; ii < Nb_blast_out; ii++) {
		for(ll=0; ll < (2*MAXSEQNAME+MAXLENINT+1); ll++){
			if (*(p+kk) =='\0') {
				printf("EXIT:\nUnexpected end of pointer TMP_BLAST!\n");
				exit(1);
				}
			ligne[ll]=*(p+kk);	
			kk++;	 
	 		}
		ligne[ll-1]='\0';
		/*printf("%d - %d: ligne [%s]\n",kk, ii,ligne);	*/
		Nieme_mot(ligne, 1, "\n \t", mn1);
		Nieme_mot(ligne, 2, "\n \t", mn2);
	
		for(ll=0; ll < (2*MAXSEQNAME+MAXLENINT+1); ll++){
			if (*(p+first*(2*MAXSEQNAME+MAXLENINT+1)+ll) =='\0') {
				printf("EXIT:\nUnexpected end of pointer TMP_BLAST!\n");
				exit(1);
				}
			ligne[ll]=*(p+first*(2*MAXSEQNAME+MAXLENINT+1)+ll);	;	 
	 	}
		ligne[ll-1]='\0';
		Nieme_mot(ligne, 1, "\n \t", firstmn1);
		Nieme_mot(ligne, 2, "\n \t", firstmn2);
	
		jj = abs(strcmp(mn1,firstmn1)) + abs(strcmp(mn2,firstmn2));
	
		if(jj != 0 || ii == (Nb_blast_out - 1))  {

		analblast_mmap_tab(first, ii - 1, test);
		
		if(test == COMP) update_fam_comp();
		else update_fam_part();
	
		first = ii;
		}
	}

	printf("End buildfam...\n");

}



/*====================================================================*/
/*====================== update_fam_comp  ============================*/
/*====================================================================*/

void update_fam_comp(void)
{
	int numq, nums, ii, jj, kk, xx, fams, famq;
	if(bla2.HIT[0].sel != GLOBAL) return;

	numq = bla.numq;
	nums = bla.HIT[0].nums;

 	/*fprintf(stderr, "%d %d \n",numq, nums);*/

	if(db_seq[nums].comp != COMP) return;
	if(db_seq[numq].comp != COMP) return;

	fams = db_seq[nums].fam;
	famq = db_seq[numq].fam;

	if(fams == famq) return;

	kk = db_fam[fams].nb;

	if(kk <= MAXFAMSEQ) {
		/*if(kk < 0) {*/
	    	for(jj=0; jj< kk; jj++) {
			ii = db_fam[fams].list[jj];
			if(ii == -1) {
				fprintf(stderr, "EXIT: error 1001!\n");
				exit(1);
				}
			if(db_seq[ii].fam != fams) {
				fprintf(stderr, "EXIT: error 1002!\n");
				exit(1);
				}
			db_seq[ii].fam = famq;
			db_fam[fams].nb = db_fam[fams].nb - 1;
			db_fam[fams].list[jj] = -1;
			xx = db_fam[famq].nb;
			if(xx < MAXFAMSEQ) db_fam[famq].list[xx] = ii;
				db_fam[famq].nb = xx + 1;
	
			}
   		}
	else {
    		for(ii=0; ii<Nb_db_seq; ii++) 
			if(db_seq[ii].fam == fams) {
				db_seq[ii].fam = famq;
				db_fam[fams].nb = db_fam[fams].nb - 1;
				db_fam[famq].nb = db_fam[famq].nb + 1;
				}
   		}


}




/*====================================================================*/
/*====================== update_fam_part  ============================*/
/*====================================================================*/


void update_fam_part(void)
{
	int numq, nums, ii;
	float pover;

/*
2eme tour de classification, en se basant sur la 1ere, mais permettant
cette fois des differences de longueur => traitement des sequences
partielles : si une sequence partielle est incluse dans une complete,
on la classe dans sa famille. 

!! dans ce cas une meme proteine peut appartenir a
N familles !!
 compter nb de famille, allouer dynamiquement

18/01/2002: changement: les sequences partielles sont classees
dans une seule famille, celle avec laquelle est a le meilleur score
total - apres suppression des overlaps et blocs  non consistants.

*/

	/* si les criteres de similarite ne sont pas remplis :  return */
	if(bla2.HIT[0].sel == REJECTED) return;

	numq = bla.numq;
	nums = bla.HIT[0].nums;

	/* si les 2 sequences sont completes (deja traite) :  return */
	if(db_seq[nums].comp == COMP && db_seq[numq].comp == COMP) return;

	/* si les 2 sequences sont partielles (non-traite) :  return */
	if(db_seq[nums].comp != COMP && db_seq[numq].comp != COMP) return;


	/* si la sequence query est partielle  :   */
	if(db_seq[numq].comp != COMP) {
		pover = (float) db_seq[numq].lg / db_seq[nums].lg;
		/* si seq. partielle trop courte, on ne la classe pas */
		if(db_seq[numq].lg < minplen || pover < minpov) return;
		if(bla2.HIT[0].sel == GLOBAL || bla2.HIT[0].sel == INCLUDED){
		  /*printf("adding %d %d with score %d \n:", numq, nums,  bla2.HIT[0].sco);*/
		  add_fam_part(db_seq[nums].fam, numq, bla2.HIT[0].sco);
		}
		}


	/* si la sequence subject est partielle  :   */
	if(db_seq[nums].comp != COMP) {
		pover = (float) db_seq[nums].lg / db_seq[numq].lg;
		/* si seq. partielle trop courte, on ne la classe pas */
		if(db_seq[nums].lg < minplen || pover < minpov) return;
		if(bla2.HIT[0].sel == GLOBAL || bla2.HIT[0].sel == CONTAIN){
		  /*printf("adding %d %d with score %d \n:", nums, numq,  bla2.HIT[0].sco);*/
		  add_fam_part(db_seq[numq].fam, nums, bla2.HIT[0].sco);
		}
		}


}


/*====================================================================*/
/*========================= renum_fam  ===============================*/
/*====================================================================*/

void renum_fam(void) 
{
	int ii, old = 0, jj=0;
	printf("Begin renum ...\n");

	/* sort seq. by family number */
	qsort(	db_seq, 
		Nb_db_seq, 
		sizeof(db_seq[0]), 
		sort_db_fam);

	for(ii=0; ii< Nb_db_seq; ii++) {
		if(db_seq[ii].fam != old) {
 			jj++;
			old = db_seq[ii].fam;
			}
		db_seq[ii].fam = jj;
		}

	/* re-sort seq. by alphabetical order */
	qsort(	db_seq, 
		Nb_db_seq, 
		sizeof(db_seq[0]), 
		sort_db_seq);

	printf("End renum ...\n");
}



/*====================================================================*/
/*======================== sort_db_fam  ==============================*/
/*====================================================================*/


/* sort seq. by family n */

int sort_db_fam(const void *a ,const void *b)
{
	struct SEQ *aa, *bb;
	int test;

	aa = (struct SEQ *) a;
	bb = (struct SEQ *) b;

	test = aa->fam - bb->fam;

	if(test == 0) test = strcmp(aa->mn, bb->mn);

	return(test); 

}

/*====================================================================*/
/*======================== add_fam_part  ==============================*/
/*====================================================================*/

void add_fam_part(int famp, int num, int sco)
{


	if(db_seq[num].nfamp == 0) {
		db_seq[num].famp = (int *)check_alloc(1,sizeof(int));
		}

	db_seq[num].nfamp = 1;

	if(sco > db_seq[num].scomax) {
	  /*printf(" selected\n");*/
	  db_seq[num].famp[0] = famp;
	  // BUG fixed
	  db_seq[num].scomax  = sco;
	}


/* 18/01/2002: changement: je ne garde que la famille de meilleur score
int ii, jj;

ii = db_seq[num].nfamp;

if(ii == 0) {
	db_seq[num].famp = (int *)check_alloc(PAS,sizeof(int));
	}

/ si le numero est deja donne, on arrete  /
for(jj=0; jj<ii; jj++) if(db_seq[num].famp[jj] == famp) return;

/ sinon on l'ajoute /
db_seq[num].famp[ii] = famp;

db_seq[num].nfamp += 1;

if(db_seq[num].nfamp % PAS == 0) realloc_famp(num);
*/

}


/*====================================================================*/
/*======================== realloc_famp  ==============================*/
/*====================================================================*/

void realloc_famp(int num)
{
	int *fam;
	int nfam, ii;

	nfam = db_seq[num].nfamp;


	fam = (int *) check_alloc(nfam,sizeof(int));
	for(ii = 0; ii < nfam; ii++) fam[ii] = db_seq[num].famp[ii];

	free((char *) db_seq[num].famp);

	db_seq[num].famp = (int *) check_alloc((nfam + PAS),sizeof(int));
	for(ii = 0; ii < nfam; ii++)  db_seq[num].famp[ii] = fam[ii];

	free((char *) fam);

}
/********************** end realloc_famp ********************************/








